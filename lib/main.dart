import 'package:flutter/material.dart';

void main(){
  runApp(HelloFlutterApp());
}
class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี Flutter";
String japanishGreeting = "Ohayo Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context){
    return MaterialApp(debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text("Hello Flutter"),
        leading: Icon(Icons.home),
        actions: <Widget>[
          IconButton(onPressed: (){
            setState(() {
              displayText = displayText == englishGreeting?
                  spanishGreeting:englishGreeting;
            });
          }, icon: Icon(Icons.flag_circle_rounded)),
          IconButton(onPressed: (){
            setState(() {
              displayText = displayText == englishGreeting?
                  thaiGreeting:englishGreeting;
            });
          }, icon: Icon(Icons.flag_circle_outlined)),
          IconButton(onPressed: (){
            setState(() {
              displayText = displayText == englishGreeting?
                  japanishGreeting:englishGreeting;
            });
          }, icon: Icon(Icons.flag)),
        ]
      ),
      body: Center(
        child: Text(
          displayText,
          style: TextStyle(fontSize: 45,color: Colors.blueGrey),
        ),
      ),
    ),
    );
  }
}
//safeArea =>
// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: (){},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 40,color: Colors.indigo),
//           ),
//         ),
//
//       ),
//
//     );
//   }
//
// }
